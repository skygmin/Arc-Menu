# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-26 18:17-0400\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: menu.js:497 menuWidgets.js:967
msgid "Arc Menu Settings"
msgstr ""

#: menu.js:506
msgid "Arc Menu on GitLab"
msgstr ""

#: menu.js:511
msgid "About Arc Menu"
msgstr ""

#: menu.js:519
msgid "Dash to Panel Settings"
msgstr ""

#: menuWidgets.js:126
msgid "Open Folder Location"
msgstr ""

#: menuWidgets.js:142
msgid "Current Windows:"
msgstr ""

#: menuWidgets.js:161
msgid "New Window"
msgstr ""

#: menuWidgets.js:171
msgid "Launch using Dedicated Graphics Card"
msgstr ""

#: menuWidgets.js:194
msgid "Remove from Favorites"
msgstr ""

#: menuWidgets.js:200
msgid "Add to Favorites"
msgstr ""

#: menuWidgets.js:221 menuWidgets.js:273
msgid "Unpin from Arc Menu"
msgstr ""

#: menuWidgets.js:235
msgid "Pin to Arc Menu"
msgstr ""

#: menuWidgets.js:248
msgid "Show Details"
msgstr ""

#: menuWidgets.js:420 prefs.js:3012
msgid "Activities Overview"
msgstr ""

#: menuWidgets.js:599
msgid "Files"
msgstr ""

#: menuWidgets.js:611 prefs.js:3012
msgid "Software"
msgstr ""

#: menuWidgets.js:630 menuWidgets.js:968 prefs.js:3012
msgid "Terminal"
msgstr ""

#: menuWidgets.js:643 prefs.js:3012
msgid "Settings"
msgstr ""

#: menuWidgets.js:655
msgid "Users"
msgstr ""

#: menuWidgets.js:667 prefs.js:3110
msgid "Power Off"
msgstr ""

#: menuWidgets.js:680 prefs.js:3092
msgid "Log Out"
msgstr ""

#: menuWidgets.js:693 prefs.js:3056
msgid "Suspend"
msgstr ""

#: menuWidgets.js:711 prefs.js:3074
msgid "Lock"
msgstr ""

#: menuWidgets.js:735
msgid "Back"
msgstr ""

#: menuWidgets.js:803 menuWidgets.js:1488 menuWidgets.js:1620
#: menuWidgets.js:1786
msgid "All Programs"
msgstr ""

#: menuWidgets.js:1488 menuWidgets.js:1620 menuWidgets.js:1786
msgid "Favorites"
msgstr ""

#: menuWidgets.js:1491 menuWidgets.js:1623 menuWidgets.js:1789
msgid "Frequent Apps"
msgstr ""

#: menuWidgets.js:1952
msgid "Type to search…"
msgstr ""

#: menuWidgets.js:2121
msgid "Applications"
msgstr ""

#: placeDisplay.js:146
#, javascript-format
msgid "Failed to launch “%s”"
msgstr ""

#: placeDisplay.js:161
#, javascript-format
msgid "Failed to mount volume for “%s”"
msgstr ""

#: placeDisplay.js:245 placeDisplay.js:269 prefs.js:2916
msgid "Computer"
msgstr ""

#: placeDisplay.js:334
#, javascript-format
msgid "Ejecting drive “%s” failed:"
msgstr ""

#: placeDisplay.js:460 prefs.js:2916
msgid "Home"
msgstr ""

#: prefs.js:43 prefs.js:679
msgid "Pinned Apps"
msgstr ""

#: prefs.js:48
msgid "Your Pinned Apps:"
msgstr ""

#: prefs.js:72
msgid "Add More Apps"
msgstr ""

#: prefs.js:116
msgid "Add Custom Shortcut"
msgstr ""

#: prefs.js:150 prefs.js:506
msgid "Save"
msgstr ""

#: prefs.js:303
msgid "Select Apps to add to Pinned Apps List"
msgstr ""

#: prefs.js:323 prefs.js:506
msgid "Add"
msgstr ""

#: prefs.js:435
msgid "Edit Pinned App"
msgstr ""

#: prefs.js:435
msgid "Add a Custom Shortcut"
msgstr ""

#: prefs.js:445
msgid "Shortcut Name:"
msgstr ""

#: prefs.js:460
msgid "Icon:"
msgstr ""

#: prefs.js:473 prefs.js:1119
msgid "Please select an image icon"
msgstr ""

#: prefs.js:492
msgid "Terminal Command:"
msgstr ""

#: prefs.js:550
msgid "General"
msgstr ""

#: prefs.js:559
msgid "Menu Button Position in Panel"
msgstr ""

#: prefs.js:566
msgid "Left"
msgstr ""

#: prefs.js:569
msgid "Center"
msgstr ""

#: prefs.js:573
msgid "Right"
msgstr ""

#: prefs.js:611
msgid "Display on all monitors when using Dash to Panel"
msgstr ""

#: prefs.js:632
msgid "Disable Tooltips"
msgstr ""

#: prefs.js:654
msgid "Disable activities hot corner"
msgstr ""

#: prefs.js:673
msgid "Choose Arc Menus Default View"
msgstr ""

#: prefs.js:680
msgid "Categories List"
msgstr ""

#: prefs.js:701
msgid "Hotkey activation"
msgstr ""

#: prefs.js:707
msgid "Key Release"
msgstr ""

#: prefs.js:708
msgid "Key Press"
msgstr ""

#: prefs.js:732
msgid "Set Menu Hotkey"
msgstr ""

#: prefs.js:742
msgid "Left Super Key"
msgstr ""

#: prefs.js:748
msgid "Right Super Key"
msgstr ""

#: prefs.js:753
msgid "Custom"
msgstr ""

#: prefs.js:758
msgid "None"
msgstr ""

#: prefs.js:853
msgid "Current Hotkey"
msgstr ""

#: prefs.js:880
msgid "Set Custom Shortcut"
msgstr ""

#: prefs.js:889
msgid "Press a key"
msgstr ""

#: prefs.js:914
msgid "Modifiers"
msgstr ""

#: prefs.js:919
msgid "Ctrl"
msgstr ""

#: prefs.js:924
msgid "Super"
msgstr ""

#: prefs.js:928
msgid "Shift"
msgstr ""

#: prefs.js:932
msgid "Alt"
msgstr ""

#: prefs.js:996 prefs.js:1703 prefs.js:1941 prefs.js:2184 prefs.js:2831
msgid "Apply"
msgstr ""

#: prefs.js:1033 prefs.js:1056 prefs.js:1328
msgid "Menu Button Appearance"
msgstr ""

#: prefs.js:1044
msgid "Icon"
msgstr ""

#: prefs.js:1045
msgid "Text"
msgstr ""

#: prefs.js:1046
msgid "Icon and Text"
msgstr ""

#: prefs.js:1047
msgid "Text and Icon"
msgstr ""

#: prefs.js:1070
msgid "Menu Button Text"
msgstr ""

#: prefs.js:1090
msgid "Arrow beside Menu Button"
msgstr ""

#: prefs.js:1109
msgid "Menu Button Icon"
msgstr ""

#: prefs.js:1124
msgid "Arc Menu Icon"
msgstr ""

#: prefs.js:1125
msgid "System Icon"
msgstr ""

#: prefs.js:1126
msgid "Custom Icon"
msgstr ""

#: prefs.js:1164
msgid "Icon Size"
msgstr ""

#: prefs.js:1196
msgid "Icon Color"
msgstr ""

#: prefs.js:1217
msgid "Active Icon Color"
msgstr ""

#: prefs.js:1237
msgid ""
"Icon color options will only work with files ending with \"-symbolic.svg\""
msgstr ""

#: prefs.js:1250 prefs.js:1908 prefs.js:2777
msgid "Reset"
msgstr ""

#: prefs.js:1306
msgid "Appearance"
msgstr ""

#: prefs.js:1356 prefs.js:1773
msgid "Customize Arc Menu Appearance"
msgstr ""

#: prefs.js:1406 prefs.js:2341
msgid "Override Arc Menu Theme"
msgstr ""

#: prefs.js:1471
msgid "Current Color Theme"
msgstr ""

#: prefs.js:1495
msgid "Choose Avatar Icon Shape"
msgstr ""

#: prefs.js:1500
msgid "Circular"
msgstr ""

#: prefs.js:1501
msgid "Square"
msgstr ""

#: prefs.js:1519
msgid "Choose Menu Layout"
msgstr ""

#: prefs.js:1574
msgid "Current Layout"
msgstr ""

#: prefs.js:1594
msgid "Each layout is different in behaviour and style."
msgstr ""

#: prefs.js:1595
msgid "All layouts can be modified by customization settings."
msgstr ""

#: prefs.js:1596
msgid "However, some settings are specifc to Arc Menu layout."
msgstr ""

#: prefs.js:1660
msgid "Menu style chooser"
msgstr ""

#: prefs.js:1669
msgid "Arc Menu Layout"
msgstr ""

#: prefs.js:1791
msgid "Menu Height"
msgstr ""

#: prefs.js:1823
msgid "Left-Panel Width"
msgstr ""

#: prefs.js:1848
msgid "Large Application Icons"
msgstr ""

#: prefs.js:1867 prefs.js:2727
msgid "Enable Vertical Separator"
msgstr ""

#: prefs.js:1886 prefs.js:2751
msgid "Separator Color"
msgstr ""

#: prefs.js:1976
msgid "Color Theme Name"
msgstr ""

#: prefs.js:1983
msgid "Name:"
msgstr ""

#: prefs.js:2009
msgid "Save Theme"
msgstr ""

#: prefs.js:2041
msgid "Select Themes to Import"
msgstr ""

#: prefs.js:2041
msgid "Select Themes to Export"
msgstr ""

#: prefs.js:2063
msgid "Import"
msgstr ""

#: prefs.js:2063
msgid "Export"
msgstr ""

#: prefs.js:2105
msgid "Select All"
msgstr ""

#: prefs.js:2165
msgid "Manage Themes"
msgstr ""

#: prefs.js:2354
msgid "Color Theme Presets"
msgstr ""

#: prefs.js:2364
msgid "Save as Preset"
msgstr ""

#: prefs.js:2459
msgid "Manage Presets"
msgstr ""

#: prefs.js:2490
msgid "Menu Background Color"
msgstr ""

#: prefs.js:2515
msgid "Menu Foreground Color"
msgstr ""

#: prefs.js:2537
msgid "Font Size"
msgstr ""

#: prefs.js:2565
msgid "Border Color"
msgstr ""

#: prefs.js:2588
msgid "Border Size"
msgstr ""

#: prefs.js:2617
msgid "Highlighted Item Color"
msgstr ""

#: prefs.js:2641
msgid "Corner Radius"
msgstr ""

#: prefs.js:2670
msgid "Menu Arrow Size"
msgstr ""

#: prefs.js:2699
msgid "Menu Displacement"
msgstr ""

#: prefs.js:2899
msgid "Configure"
msgstr ""

#: prefs.js:2906
msgid "Enable/Disable shortcuts"
msgstr ""

#: prefs.js:2916
msgid "Documents"
msgstr ""

#: prefs.js:2916
msgid "Downloads"
msgstr ""

#: prefs.js:2916
msgid "Music"
msgstr ""

#: prefs.js:2916
msgid "Pictures"
msgstr ""

#: prefs.js:2916
msgid "Videos"
msgstr ""

#: prefs.js:2917
msgid "Network"
msgstr ""

#: prefs.js:2961
msgid "External Devices"
msgstr ""

#: prefs.js:2984
msgid "Bookmarks"
msgstr ""

#: prefs.js:3012
msgid "Tweaks"
msgstr ""

#: prefs.js:3143
msgid "Misc"
msgstr ""

#: prefs.js:3149
msgid "Export and Import Arc Menu Settings"
msgstr ""

#: prefs.js:3156
msgid "All current Arc Menu settings will be changed when importing from file."
msgstr ""

#: prefs.js:3157
msgid "This includes all saved pinned apps."
msgstr ""

#: prefs.js:3166
msgid "Import from File"
msgstr ""

#: prefs.js:3172
msgid "Import settings"
msgstr ""

#: prefs.js:3201
msgid "Export to File"
msgstr ""

#: prefs.js:3208
msgid "Export settings"
msgstr ""

#: prefs.js:3239
msgid "Color Theme Presets - Export/Import"
msgstr ""

#: prefs.js:3258
msgid "Imported theme presets are located on the Appearance Tab"
msgstr ""

#: prefs.js:3259
msgid "in Override Arc Menu Theme"
msgstr ""

#: prefs.js:3270 prefs.js:3276
msgid "Import Theme Preset"
msgstr ""

#: prefs.js:3316 prefs.js:3327
msgid "Export Theme Preset"
msgstr ""

#: prefs.js:3404
msgid "About"
msgstr ""

#: prefs.js:3432
msgid "Arc-Menu"
msgstr ""

#: prefs.js:3437
msgid "version: "
msgstr ""

#: prefs.js:3441
msgid "The new applications menu for Gnome 3"
msgstr ""

#: prefs.js:3445
msgid "GitLab Page"
msgstr ""

#: searchGrid.js:692 search.js:658
msgid "Searching..."
msgstr ""

#: searchGrid.js:694 search.js:660
msgid "No results."
msgstr ""

#: searchGrid.js:793 search.js:741
#, javascript-format
msgid "%d more"
msgid_plural "%d more"
msgstr[0] ""
msgstr[1] ""